﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices;

namespace ADTest
{
    class Program
    {
        static void Main(string[] args)
        {

            DirectoryEntry rootEntry = new DirectoryEntry("LDAP://bizlink.nsw.gov.au");
            rootEntry.AuthenticationType = AuthenticationTypes.Secure;
            DirectorySearcher search = new DirectorySearcher(rootEntry);

            search.SearchScope = SearchScope.Subtree;
            search.Filter = "(ObjectClass=user)";

            foreach (SearchResult adObject in search.FindAll())
            {
                Console.WriteLine("CN=" + adObject.Properties["CN"][0] + " Path=" + adObject.Path);
            }

            Console.WriteLine();
            Console.WriteLine("Hit any key to end");

            Console.ReadKey();

        }
    }
}
